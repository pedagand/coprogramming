# Groupe de travail Coprogrammation

## Thèmes

Le groupe de travail Coprogrammation regroupe des chercheurs et chercheuses de
Paris et ses environs intéressés par la programmation, le calcul et le
raisonnement mettant en jeu des objets infinis.

L'exemple canonique est celui des flux (_streams_) infinis de données, qui a été
étudié dans de nombreux contextes : programmation réactive et synchrone, théorie
des types gardés, raisonnement coinductif, sémantiques coalgébriques, preuves
circulaires, etc.

Un des objectifs du groupe de travail Coprogrammation est de contribuer à la
diffusion de ces connaissances entre chercheurs et chercheuses issus de
différents contextes.

## Fonctionnement

Le groupe de travail se réunit une fois par mois. Les séances sont publiques.

Il dispose d'une [liste de
diffusion](https://listes.irif.fr/sympa/info/coprogramming) en accès libre,
n'hésitez pas à vous y inscrire.

## Réunions

| Date     | Lieu                                  | Horaire | Exposés              |
|----------|---------------------------------------|---------|----------------------|
| 22/05/23 | Université Paris Cité (Paris XIIIème) | 14h-16h | Dagand, Thomé, Pesin |

### 22/05/23

**Lieu** : Université Paris Cité, campus Grands Moulins, [bâtiment Olympe de
Gouges](https://www.openstreetmap.org/way/188043992), salle de séminaire 147.

| Horaire     | Exposé                                                                                     |
|-------------|--------------------------------------------------------------------------------------------|
| 14h00-15h00 | Reader's digest: "Let's see how things unfold" -- Pierre-Évariste-Dagand                   |
| 15h00-15h20 | How to conciliate critical embedded systems with asynchronous programming? -- Émilie Thomé |
| 15h20-16h00 | Raisonnement co-inductif dans Coq pour un compilateur vérifié -- Basile Pesin              |
| 16h00-16h30 | échanges entre participants et participantes                                               |

#### Reader's digest: "Let's See How Things Unfold" ; Pierre-Évariste Dagand, IRIF

McBride wrote

> To combine programming and reasoning, or just to program with greater
> precision, we might look to the proof assistants and functional languages
> based on intensional type theories [...]. But we are in for a nasty shock if
> we do. Coinduction in Coq is broken: computation does not preserve
> type. Coinduction in Agda is weak : dependent observations are disallowed, so
> whilst we can unfold a process, we cannot see that it yields its unfolding.

Equipped with a few Emacs buffers, I will substantiate these claims using
concrete examples.

#### How to conciliate critical embedded systems with asynchronous programming? ; Émilie Thomé, Renault Software Factory

We introduce the ongoing design of LanGRust, a domain-specific language~(DSL)
for the automotive industry compiling in Rust, based on asynchronous
event-reactive programming principles, alongside synchronous time-reactive
guarantees.

Synchronous languages ensure an execution in bounded memory and time. These
time-reactive properties make them well-suited for the development of critical
systems. In the synchronous model, time is a succession of periodical ticks on
which the program performs computations.

Some event-driven software components (such as the ADAS Sensor Fusion
processing) may have to deal with a tremendous number of aperiodic events, which
doesn't scale well with legacy synchronous schedulers.

The goal is to find the right tradeoff between the efficiency of these
asynchronous techniques and the properties required when developing embedded
systems (determinism, bounded memory and time, ...).

#### Raisonnement co-inductif dans Coq pour un compilateur vérifié ; Basile Pesin, PARKAS

Le projet Vélus est un compilateur vérifié pour un langage synchrone à flots de
données, basé sur Lustre, Lucid-Synchrone et Scade 6, implémenté dans
l'assistant de preuve Coq.  Il comprend une sémantique formelle pour le langage
d'entrée, modélisée par un ensemble de relations sur des flots de données
infinis.  On présentera ce modèle et sa formalisation en Coq, qui utilise des
structures de données co-inductives.  La manipulation de ces structures peut
parfois rendre le raisonnement plus difficile; on montrera les astuces pratiques
qu'on utilise pour mitiger ces difficultés.

Mon sujet de thèse consiste à étendre Vélus avec des structures de contrôle de
haut niveau permettant l'implémentation de comportements modaux: blocs switch,
variables partagées, blocs reset et enfin machines à états.  La sémantique de
ces constructions de contrôle est modélisée par le sous-échantillonnage des
environnements d'exécution.  En particulier, la sémantique d'un bloc de reset
dépend de l'existence d'une infinité de sémantiques sous-échantillonnées pour
les blocs sous-jacents.  On discutera de deux difficultés que cette modélisation
introduit dans la preuve de correction du compilateur.  A notre connaissance, le
seul moyen de les résoudre est d'introduire deux axiomes mathématiques dans la
logique de Coq, et de construire un raisonnement complexe sur ces axiomes.
